package com.app.library.service.book;

import com.app.library.dto.BookDto;
import com.app.library.repository.BookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookServiceTest {

    @Autowired
    private BookServiceImpl bookService;

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void shouldSaveBook() {
        //given
        BookDto bookDto = BookDto.builder().title("Title")
                .author("Author")
                .isbn("ISBN")
                .build();

        //when
        BookDto book = bookService.saveBook(bookDto);

        //then
        Assertions.assertNotNull(bookRepository.getById(book.getId()));
    }
}
