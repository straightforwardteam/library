package com.app.library.controller;

import com.app.library.dto.BookDto;
import com.app.library.service.book.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * Controller for book operations handling
 */
@Controller
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    /**
     * Get main page
     * @param model site model
     * @param form bool param, depends on value form for book adding appears
     * @return view
     */
    @GetMapping("/")
    public String index(Model model,
                        @RequestParam(name="form", required=false, defaultValue="false") Boolean form,
                        @RequestParam(name = "pageNumber", required = false, defaultValue = "1") Integer pageNumber) {

        Page<BookDto> page = bookService.getBooksPaginated(pageNumber, 10);

        model.addAttribute("currentPage", pageNumber);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalBooks", page.getTotalElements());
        model.addAttribute("bookDto", new BookDto());
        model.addAttribute("form", form);
        model.addAttribute("books", page.getContent());
        return "index";
    }

    /**
     * Save new book
     * @param bookDto dto object for Book entity
     * @param bindingResult validation result
     * @param model site model
     * @return view
     */
    @PostMapping("/saveBook")
    public String addBook(@Valid @ModelAttribute("bookDto") BookDto bookDto, BindingResult bindingResult, Model model) {
        model.addAttribute("form", true);
        if (bindingResult.hasErrors()) {
            return "index";
        }
        bookService.saveBook(bookDto);
        return "redirect:/";
    }
}
