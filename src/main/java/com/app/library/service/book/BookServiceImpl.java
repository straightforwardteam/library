package com.app.library.service.book;

import com.app.library.dto.BookDto;
import com.app.library.mapper.BookMapper;
import com.app.library.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for operations associated with book entity
 */
@Service
@Transactional
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    /**
     * Save new book
     * @param bookDto book entity dto object
     * @return book entity dto object
     */
    @Override
    public BookDto saveBook(BookDto bookDto) {
        return BookMapper.toDto(bookRepository.save(BookMapper.toNewEntity(bookDto)));
    }

    /**
     * Get all books paginated
     * @return list of book entity dto objects
     */
    @Override
    public Page<BookDto> getBooksPaginated(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize);
        return bookRepository.findAll(pageable).map(BookMapper::toDto);
    }
}
