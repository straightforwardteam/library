package com.app.library.service.book;

import com.app.library.dto.BookDto;
import org.springframework.data.domain.Page;

/**
 * Interface for book service
 */
public interface BookService {

    BookDto saveBook(BookDto bookDto);

    Page<BookDto> getBooksPaginated(Integer pageNumber, Integer pageSize);
}
