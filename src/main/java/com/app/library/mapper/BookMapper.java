package com.app.library.mapper;

import com.app.library.dto.BookDto;
import com.app.library.entity.Book;

/**
 * Mapper for Book
 */
public class BookMapper {

    /**
     * Create dto from entity
     * @param book book entity
     * @return dto object
     */
    public static BookDto toDto(Book book) {
        return BookDto.builder()
                .id(book.getId())
                .author(book.getAuthor())
                .title(book.getTitle())
                .isbn(book.getIsbn())
                .build();
    }

    /**
     * Create new entity from book entity dto
     * @param bookDto book entity dto
     * @return book entity
     */
    public static Book toNewEntity(BookDto bookDto) {
        return Book.builder()
                .author(bookDto.getAuthor())
                .title(bookDto.getTitle())
                .isbn(bookDto.getIsbn())
                .build();
    }
}
