package com.app.library.repository;

import com.app.library.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Book repository
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
}
