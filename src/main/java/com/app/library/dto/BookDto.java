package com.app.library.dto;

import lombok.*;

import javax.validation.constraints.Pattern;

/**
 * Dto object for Book entity
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDto {

    private Long id;

    @Pattern(regexp = "^A(.*)", message = "Author must start with the letter 'A'")
    private String author;

    private String title;

    private String isbn;
}
